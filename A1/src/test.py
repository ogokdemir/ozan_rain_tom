# -*- coding: utf-8 -*-
"""
Created on Tue Sep 18 11:44:39 2018

@author: Ozan Gokdemir
"""

import csv 

def read_and_process_file(filepath):
    with open(filepath, newline='') as csvfile:
        reader = csv.reader(csvfile)
        data = []
        for row in reader:
            data.append(row)
          
    return list(map(frozenset,data))
    
#print(read_and_process_file("C:\\Users\\Ozan Gokdemir\\Desktop\\DataMining2018\\A1\\data\\GroceryStoreDataSet.csv"))

#finds the single item candidates in the dataset. 
def findCandidates1(dataset):
    cands = []
    for trans in dataset: 
        for item in trans:
            if not [item] in cands:
                cands.append([item])
    
    cands.sort()
    #returning frozensets so that these itemsets can be used as the keys of a dictionary(a.k.a python hashtable).
    return list(map(frozenset, cands))
    
    
#print(findCandidates1(database))


#an attempt to optimize the the algorithm. 
#idea is that a transaction that does not contain any frequent k-itemset is useless in subsequent scans.
def transaction_reduction(frequentItemsets, dataset):
    for trans in dataset:
        count = 0 
        for freqSet in frequentItemsets:
            if freqSet.issubset(trans):
                count+=1 
        if count == 0:
            dataset.remove(trans)


#filters the candidates that satisfy the support requirement, keeps the valid ones and dumps the rest. 
def findFrequentItemsets(dataset, supCriteria):
    frequentItemsets = [] #stores only the candidates that satisy the support threshold. 
    cands = findCandidates1(dataset)
    while(len(cands)>0):
        temp = []
        for c in cands: 
            if support(c, dataset) >= supCriteria:
                temp.append(frozenset(c))
                frequentItemsets.append(c)             
        transaction_reduction(frequentItemsets, dataset)
        cands = []
        
        for t in temp:
            temp.remove(t)
            for u in range(len(temp)):
                miniset = frozenset(temp[u].union(t))
                cands.append(miniset)
    return frequentItemsets






supportCache = {}

def support(itemset, dataset):
    
    if(itemset in supportCache):
        return supportCache[itemset]
    else:
        countOfAppearance = 0  
        for trans in dataset:
            if itemset.issubset(trans):
                countOfAppearance += 1
        
        supportCache[itemset] = countOfAppearance
        return countOfAppearance


def confidence(precedent, antecedent, database):  
    testset = precedent.union(antecedent)
    return support(testset,database) / support(precedent, database)



def findRules(database, minConfidence):
    frequentItems = findFrequentItemsets(database, 2)
    print(frequentItems)
    rules = []
    
    for f in frequentItems:
        frequentItems.remove(f)
        for u in range(len(frequentItems)):
            if confidence(f, frequentItems[u], database) >= minConfidence:
                if(len(f) <= len(frequentItems[u])):
                    rules.append((f , frequentItems[u]))
    return rules
    

data = read_and_process_file('C:\\Users\\Ozan Gokdemir\\Desktop\\DataMining2018\\A1\\data\\GroceryStoreDataSet.csv')

rules = list(findRules(data, 0.1))
for r in rules:
    print("s")
    print(str(r[0]) + " ===> " + str(r[1]))
    