'''
IMPORTANT NOTE FOR THE GRADER: !!! 

The main file you need to run to grade our assignment is application.py in the src folder. Rest of the files are auxillary. 

Please make sure that movielistingtxtpath is set to the directory in which movielisting.txt file resides.
Please make sure that sortedcsvpath is set to the directory in which sorted.csv file resides.

Both these variables are in the very beginning of the code, before the read_and_process_file function. 

Both the files are in the data directory in the project folder so these paths will change based on
where you download this code to.

This is the only thing you will need to alter with my code to run it. 

Please follow this form when you specify the directory of the data files:

"C:\\Users\\Ozan Gokdemir\\Desktop\\introdm\\A1\\data\\movielisting.txt"
'''

All the functions are clearly explained with comments. 
You will find clear instructions in the comments regarding running the code, changing the support and confidence requirements etc. 

This code has been confirmed to run on the ncfvm virtual machine. 


Thank you for your time and effort on grading the assignment. 


9/13/2018 8:47pm
Ozan