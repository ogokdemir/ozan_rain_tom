# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 12:28:22 2018

@author: Ozan Gokdemir
"""

import re

filepath = "C:\\Users\\Ozan Gokdemir\\Desktop\\introdm\\A1\\data\\movielisting.txt"



def processRawMovieNameList(filepath):
    
    file = open(filepath, "r")

    raw_text = file.read()

    namesList = {}

    splitList = raw_text.splitlines()
    
    for row in splitList: 
        movie = row.split(" ")
        for i in movie:
            if("(" in i):
                idx = movie.index(i)
                movName=""
                for j in range(1,idx):
                    movName+=(movie[j]+" ")
                    namesList[movie[0]] = movName
    return namesList
          
            
print(processRawMovieNameList(filepath))
