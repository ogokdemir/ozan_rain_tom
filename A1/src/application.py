# -*- coding: utf-8 -*-
"""
Created on Mon Sep 17 21:07:16 2018

@author: Ozan Gokdemir
"""


import csv
import re


'''
IMPORTANT NOTE FOR THE GRADER: !!! 

Please make sure that movielistingtxtpath is set to the directory in which movielisting.txt file resides.
Please make sure that sortedcsvpath is set to the directory in which sorted.csv file resides.

Both the files are in the data directory in the project folder so these paths will change based on
where you download this code to.

This is the only thing you will need to alter with my code to run it. 
'''

movielistingtxtpath = "C:\\Users\\Ozan Gokdemir\\Desktop\\ozan_rain_tom\\A1\\data\\movielisting.txt"
sortedcsvpath = "C:\\Users\\Ozan Gokdemir\\Desktop\\ozan_rain_tom\\A1\\data\\sorted.csv"



'''
This function reads the raw csv file in which our data is contained.
@Returns: a list of frozensets. Each of these sets represents the movies a single user watched.
In other words, each set in this returned list is a transaction we feed into the apriori algorithm.

@Params: The filepath in which sorted.csv file resides.
PLEASE MAKE SURE YOU PASS THE RIGHT DIRECTORY FOR THIS FUNCTION TO READ THE FILE.

'''
def read_and_process_file(filepath):
    with open(filepath, newline='') as csvfile:
        reader = csv.reader(csvfile)
        current_users_movie_list = []
        database = []
        current_user = 0
        for row in reader:
            if row[0] == current_user:
                current_users_movie_list.append(row[1])
            else:
                database.append(current_users_movie_list)
                current_user = row[0]
                current_users_movie_list = [row[1]]
                
         
    del database[0:2]
    return list(map(frozenset,database))
  

 


#finds the single item candidates(K1 candidates.) in the dataset.
def findCandidates1(dataset):
    print("first candidates are being found")
    cands = []
    for trans in dataset: 
        for item in trans:
            if not {item} in cands:
                cands.append({item})
    
    cands.sort()
    #returning frozensets so that these itemsets can be used as the keys of a dictionary(a.k.a python hashtable).
    return list(map(frozenset, cands))
    
    
#print(len(findCandidates1(dat)))
#print(findCandidates1(dat))

#a successful attempt to optimize the the algorithm. 
#idea is that a transaction that does not contain any frequent K(n)-itemset is useless in subsequent scans.
def transaction_reduction(frequentItemsets, dataset):
    print("transaction_reduction called")
    for trans in dataset:
        for freqSet in frequentItemsets:
            if freqSet.issubset(trans):
                break
        dataset.remove(trans)


#filters the candidates that satisfy the support requirement, keeps the valid ones and dumps the rest. 
def findFrequentItemsets(cands, dataset, supCriteria):
    print("findfrequentitemsets called.")
    frequentItemsets = [] #stores only the candidates that satisy the support threshold. 
    while(len(cands)>0):
        temp = []
        for c in cands: 
            if support(c, dataset) >= supCriteria:
                temp.append(frozenset(c))
                frequentItemsets.append(c)
        before_removal = len(dataset)        
        transaction_reduction(frequentItemsets, dataset)
        after_removal = len(dataset)
        print("transaction reduction executed, " + str(before_removal - after_removal)+ "transactions removed.")
        print(str(after_removal) + " transactions remaining.")
        cands = []
        
        for t in temp:
            temp.remove(t)
            for u in range(len(temp)):
                miniset = frozenset(temp[u].union(t))
                cands.append(miniset)
    return frequentItemsets


#Helper function to compute the tally(support) of an itemset in the database.
def support(itemset, dataset):
    count = 0 
    for trans in dataset:
        if itemset.issubset(trans):
            count += 1
    return count


#Helper function to oompute the confidence of an association rule based on the precedent and antecedent itemsets.
def confidence(precedent, antecedent, database):  
    testset = precedent.union(antecedent)
    
    return support(testset,database) / support(precedent, database)


#Computes the association rules that satiate the confidence threshold among the frequent itemsets 
def findRules(frequentItems, database, minConfidence):
    
    rules = {}
    
    for f in frequentItems:
        frequentItems.remove(f)
        for u in range(len(frequentItems)):
            conf = confidence(f, frequentItems[u], database)
            if conf >= minConfidence:
                if(len(f) <= len(frequentItems[u])):
                    rule = frozenset([f, frequentItems[u]])
                    rules[rule] = conf
    return rules
    


#This helper function reads and processes the raw movielisting.txt file.
#It is used in converting the movieIds in the rules to the movie names.
#param: path in which the movielisting file resides. PLEASE PASS THE DIRECTORY ON YOUR COMPUTER.
#returns: a dictionary mapping the movieId to the movie name. 
def processRawMovieNameList(filepath):
    
    file = open(filepath, "r")
    
    raw_text = file.read()  # opening and reading the movielisting.txt file.
    
    namesList = {} # the dictionary that maps the ids of movies to their names.
    
    splitList = raw_text.splitlines()
    
    for row in splitList: 
        movie = row.split(" ")
        for i in movie:
            if("(" in i):
                idx = movie.index(i)
                movName=""
                for j in range(1,idx):
                    movName+=(movie[j]+" ")
                    namesList[movie[0]] = movName
    return namesList
          


#A helper function that takes in the dictionary mapping(id --> name) from processRawMovieNameList
#and the output string from findRules() containing the rules,replaces ids in the rules with names for reading convenience.

def convertIdsToNamesInRules(dictionaryMapping, rulesString):
  
    for key in dictionaryMapping:
        rulesString = rulesString.replace(key, dictionaryMapping[key]+", ")
    return rulesString




#Client function. Test the code here.

#To change support threshold, change the 3rd argument of the findFrequentItemsets method call.

#To change the confidence threshold, change the 3rd argument of the finfRules method call.

#You should observe that decreasing the values of these thresholds will result in more association rules(with lower confidence) being computed.
def main():

    #Reading the file, processing formatted raw data into a list of transactions. 
    dat = read_and_process_file("C:\\Users\\Ozan Gokdemir\\Desktop\\DataMining2018\\A1\\data\\sorted.csv")   
    
    firstCandidates = findCandidates1(dat) # compute candidate transactions with one item in them.

    print("k1 candidates calculated, number is "+ str(len(firstCandidates)))

    #compute frequent itemsets in the data, this list of sets will include all frequent sets.
    frequentItemsets = findFrequentItemsets(firstCandidates, dat, 170)

    print("frequent itemsets calculated, number is "+ str(len(frequentItemsets)))


    #compute the association rules among the frequent itemsets, based on the confidence threshold.
    rules = findRules(frequentItemsets, dat, 0.95)

    for r in rules.keys():
        temp = list(r)
        #print(str(temp[0]) + "===>" + str(temp[1]) + " with confidence= " + str(rules[r])+"\n")

    print("rules calculated number is "+ str(len(rules)))

    
    #Readign movielisting.txt file to create a dictionary that maps movie ids to movie names.
    mapping = processRawMovieNameList(movielistingtxtpath)


    #replacing the ids in the association rules output with the movie names for reader's convenience.
    for r in rules.keys():
        temp = list(r)
        print(convertIdsToNamesInRules(
            mapping, str(temp[0]) + "===>" + str(temp[1])) + " with confidence= " + str(rules[r]))
        
    print(" ")
    print(" ")
    print("rules calculated number is "+ str(len(rules)))

    


main()
